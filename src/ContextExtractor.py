# -*- coding: UTF-8 -*-
"""
Context Extractor for WMT14 QE data

Extracts context from the source and the target text:
- input: two files (same number of sentences)
- output: tuples of (x,y) where x is a vector of word indices and y the label

Bilingual context -> bilingual features:
- fixed size word window around each word
- fixed size word window around position of word in target language (naive approach)
"""
import numpy as np
from gensim import corpora
import codecs
import re
from nltk.tokenize import word_tokenize #requires u'tokenizers/punkt/english.pickle, else use     >>> from nltk.tokenize import wordpunct_tokenize  >>> wordpunct_tokenize(s)


class ContextExtractor1(object):
	"""for task 1: extract features from both whole sentences"""
	def __init__(self, source, target, score, wordDictionary, labelDictionary, wordlevel=False, labels=None, contexts=None, features=None):
		self.source = source
		self.target = target
		self.score = score
		self.wordlevel = wordlevel
		if source is not None: #source can be None if working only on target data
			self.s = codecs.open(source, "r", "utf8")
		else:
			self.s = None
		self.t = codecs.open(target, "r", "utf8")
		self.sc = codecs.open(score, "r", "utf8")
		self.wd = wordDictionary #dictionary is for both languages
		self.ld = labelDictionary
		self.labels = labels
		self.contexts = contexts
		self.targetSents = list()
		self.sourceSents = list()
		self.useFeatures = False
		if features is not None:
			self.useFeatures = True
			self.f = codecs.open(features, "r", "utf8")

	def extract(self, targetWindow=51, sourceWindow=51):
		if self.source == None: #extract features only from target data
			print "...only working on target data"
			#TODO

		else:
			if self.wordlevel == False:
				targets = list() #collect target sentences
				for linet in self.t:
					targets.append(word_tokenize(linet.strip())) #sentence might be longer than targetWindow		
				sources = list() #collect source sentences
				for lines in self.s:
					sources.append(word_tokenize(lines.strip())) #sentence might be longer than sourceWindow		
				numberOfTargets = len(targets) #targets are sentences
				contexts = np.zeros((numberOfTargets,targetWindow+sourceWindow), dtype=int) #numpy matrix for contexts
				labels = np.zeros(numberOfTargets)
				self.targetSents = targets
				self.sourceSents = sources
				
				for i in xrange(numberOfTargets): 
					if len(targets[i])>targetWindow: #crop targets if sentence longer than window size
							targets[i] = targets[i][:targetWindow]
					if len(sources[i])>sourceWindow: #crop sources if sentence longer than window size
							sources[i] = sources[i][:sourceWindow]
					for k in xrange(targetWindow): #first fill in target features
						if k>=len(targets[i]):	
							contexts[i,k] = self.wd.token2id["PADDING"]
						else:
							try:	
								contexts[i,k] = self.wd.token2id[targets[i][k]]
							except KeyError:
								#print "Key Error:",self.targetWords[i+j]
								contexts[i,k] = self.wd.token2id["UNKNOWN"] #unknown words can occur due to tokenization problems
					for j in xrange(k,sourceWindow+targetWindow): #then continue with source features
						if j-k>=len(sources[i]):
							contexts[i,j] = self.wd.token2id["PADDING"]
						else:
							try:
								contexts[i,j] = self.wd.token2id[sources[i][j-k]]
							except KeyError:
								#print "Key Error:",self.targetWords[i+j]
								contexts[i,j] = self.wd.token2id["UNKNOWN"] #unknown words can occur due to tokenization problems
				for i,line in enumerate(self.sc):
					labels[i] = self.ld[line.strip()]
				self.labels = labels
				self.contexts = contexts
		
				if self.useFeatures: #use provided baseline features
					print "...using baseline features"
					extendedContexts = np.zeros((numberOfTargets,targetWindow+sourceWindow+17), dtype=int) #numpy matrix for contexts
					for i,line in enumerate(self.f):
						splitted = line.strip().split()
						if len(splitted) != 17:
							print "... not enough (<17) features provided for instance %d" % d
							continue
						features = [self.wd.token2id[f] for f in splitted]
						extendedContexts[i] = np.concatenate((self.contexts[i],features),axis=0) #extend contexts
					self.contexts = extendedContexts
				return self.contexts, self.labels

	def getContext(self, targetIndex):
		""" Get the extracted context for a given index of target word in sentence """
		contextForSent = [self.wd[c] for c in self.contexts[targetIndex]]
		print "context words for sentence '%s':\n %s" %(" ".join(self.targetSents[targetIndex]), str(contextForSent))
		return contextForSent
		
						

class ContextExtractor2(object):
	"""for task 2: extract features from a fixed window size around target word"""
	
	def __init__(self, source, target, wordDictionary, labelDictionary, taskIndex, wordlevel=True, labels=None, contexts=None):
		self.source = source
		self.target = target
		self.wordlevel = wordlevel
		if source is not None: #source can be None if working only on target data
			self.s = codecs.open(source, "r", "utf8")
		else:
			self.s = None
		self.t = codecs.open(target, "r", "utf8")
		self.wd = wordDictionary #dictionary is for both languages
		self.ld = labelDictionary
		self.labels = labels
		self.contexts = contexts
		self.targetWords = list() # contains tuples: ID,index,word,multi,coarse,binary
		self.sourceWords = list()
		self.taskIndex = taskIndex

	def extract(self, targetWindow=5, sourceWindow=5):
		if self.source == None: #extract features only from target data
			print "...only working on target data"
			if self.wordlevel == True: #annotation on word level
				#target file format:
				#ID\tindex\tword\tmulti\tcoarse\tbinary
				#ID is unique within file, index is within sentence, both start with 0
				targets = list()
				for line in self.t: #read target line for line -> word for word
					if len(line.split("\t"))==6:
						(Id, index, word, multi, coarse, binary) = line.split("\t")
						targets.append((Id, index, word, multi, coarse, binary.strip()))
					else:
						print "ERROR, not enough data in line", len(line.split("\t"))
				#print targetWords
				self.targetWords = targets
				targetWords = [t[2] for t in targets]	#contains only words
				#collect labels for given task			
				labels = [self.ld[t[self.taskIndex]] for t in targets]
				#print labels
				numberOfTargets = len(targetWords)
				#print len(targetWords), "target words read"
	
				contexts = np.zeros((numberOfTargets,targetWindow), dtype=int) #numpy matrix for
			
				for i in xrange(numberOfTargets): #go through words again		
				
					k = 0 #counter for features
					#collect target words within window
					for j in xrange(int(-np.floor(targetWindow/2.)),int(np.floor(targetWindow/2.)+1)): #counter for index in target words
						#print "datapoint", i
						#print "contextwindowindex", j
						#print "counter for features", k
						if i+j<0 or i+j>=numberOfTargets:
							contexts[i,k] = self.wd.token2id["PADDING"]				
							#print "added PADDING"
						else:
							#print targetWords[i+j][2]
							try:
								contexts[i,k] = self.wd.token2id[targetWords[i+j]]
							except KeyError:
								#print "Key Error:",self.targetWords[i+j]
								contexts[i,k] = self.wd.token2id["UNKNOWN"] #unknown words can occur due to tokenization problems
						k+=1
				self.labels = labels
				self.contexts = contexts
				return self.contexts, self.labels
	
		else:
			if self.wordlevel == True: #annotation on word level
				#target file format:
				#ID\tindex\tword\tmulti\tcoarse\tbinary
				#ID is unique within file, index is within sentence, both start with 0
				targets = list()
				for line in self.t: #read target line for line -> word for word
					if len(line.split("\t"))==6:
						(Id, index, word, multi, coarse, binary) = line.split("\t")
						targets.append((Id, index, word, multi, coarse, binary.strip()))
					else:
						print "ERROR, not enough data in line", len(line.split("\t"))
				#print targetWords
				self.targetWords = targets
				targetWords = [t[2] for t in targets]	#contains only words
				#collect labels for given task			
				labels = [self.ld[t[self.taskIndex]] for t in targets]
				#print labels
				numberOfTargets = len(targetWords)
				#print len(targetWords), "target words read"	
	
				sourceSentences = dict()
				for line in self.s: #read source sentence for sentence
					(Id, words) = line.split("\t",1)
					sourceSentences[int(float(Id)-0.1)] = word_tokenize(words.strip()) #.1 is removed from sentence id
					self.sourceWords.extend(word_tokenize(words.strip()))
			#	print len(self.sourceWords), "source words read"
			#	print len(sourceSentences), "source sentences read"

				contexts = np.zeros((numberOfTargets,targetWindow+sourceWindow), dtype=int) #numpy matrix for contexts for target words

				for i in xrange(numberOfTargets): #go through words again		
				
					k = 0 #counter for features
					#collect target words within window
					for j in xrange(int(-np.floor(targetWindow/2.)),int(np.floor(targetWindow/2.)+1)): #counter for index in target words
						#print "datapoint", i
						#print "contextwindowindex", j
						#print "counter for features", k
						if i+j<0 or i+j>=numberOfTargets:
							contexts[i,k] = self.wd.token2id["PADDING"]				
							#print "added PADDING"
						else:
							#print targetWords[i+j][2]
							try:
								contexts[i,k] = self.wd.token2id[targetWords[i+j]]
							except KeyError:
								#print "Key Error:",self.targetWords[i+j]
								contexts[i,k] = self.wd.token2id["UNKNOWN"] #unknown words can occur due to tokenization problems
						k+=1
					#extract source words within window
					#print len(sourceSentences)
					for j in xrange(int(-np.floor(sourceWindow/2.)),int(np.floor(sourceWindow/2.)+1)):	# extract features from source					
						targetWordIndex = int(targets[i][1])
						targetWordSentenceIndex = int(float(targets[i][0])-0.1)
						#print targetWordSentenceIndex
						if targetWordIndex+j<0 or targetWordIndex+j>=len(sourceSentences[targetWordSentenceIndex]):
							contexts[i,k] = self.wd.token2id["PADDING"]
						else:
							try:
								contexts[i,k] = self.wd.token2id[sourceSentences[targetWordSentenceIndex][targetWordIndex+j]]
							except KeyError:
								#print "Key Error:",sourceSentences[targetWordSentenceIndex][targetWordIndex+j]
								contexts[i,k] = self.wd.token2id["UNKNOWN"]
						k+=1
			#print labels #works
			#print contexts #works
			self.labels = labels
			self.contexts = contexts
			return self.contexts, self.labels

	def getContext(self, targetIndex):
		""" Get the extracted context for a given index of target word in sentence """
		word = self.targetWords[targetIndex]
		contextForWord = [self.wd[c] for c in self.contexts[targetIndex]]
		print "context words for word '%s': %s" %(self.wd[self.contexts[targetIndex][2]], str(contextForWord))
		return contextForWord
		
	
def corpus2dict(corpusfiles):
	""" From a given corpus, create a gensim dictionary for mapping words to ints """
	corpus = list()
	corpus.append(["PADDING"]) #has word index 0
	corpus.append(["UNKNOWN"]) #has word index 1
	for cf in corpusfiles:
		if cf is not None: #source can be none
			corpus.extend(preprocess(codecs.open(cf,"r","utf8").readlines()))
	wordDictionary = corpora.Dictionary(corpus)
	return wordDictionary

def preprocess(docs):
	""" Preprocess a document: tokenize words """
	#texts = [[re.sub('[^A-Za-z0-9]+', '', word) for word in doc.lower().split()] for doc in docs] #no substitution of non-alphabetic characters possible, since they are considered as target words, no lower() since capitalisation is a type of translation error
	texts = [[word for word in word_tokenize(doc)] for doc in docs] #nltk tokenizer 
	return texts
	

# -*- coding: UTF-8 -*-

from ContextExtractor import ContextExtractor2, ContextExtractor1, corpus2dict
import theano
import theano.tensor as T
import numpy as np

"""WMT 14 Quality Estimation Tasks"""

class WMT14QETask1_1(object):
	"""
	Task 1-1: Sentence-level
	Predict the quality of the translation for each sentence.
	Three labels: 1,2,3
	"""
	#read from 3 files: target, source, score
	#how to predict a sentence?
	#take whole sentences as features
	#context size? longest sentence in de-en setting is 51 words long -> naive approach
	def __init__(self, languagePair, pathToTestData, pathToTrainData, targetWindowSize=51, sourceWindowSize=51, onlyTarget=False, baselineFeatures=False):
		self.languagePair = languagePair
		self.test_source = pathToTestData+"/"+self.languagePair+"_source.test"
		self.test_target = pathToTestData+"/"+self.languagePair+"_target.test"
		self.test_score =  pathToTestData+"/"+self.languagePair+"_score.test"
		self.train_source = pathToTrainData+"/"+self.languagePair+"_source.train"
		self.train_target = pathToTrainData+"/"+self.languagePair+"_target.train"
		self.train_score = pathToTrainData+"/"+self.languagePair+"_score.train"
		self.baselineFeatures = baselineFeatures
		self.data = [self.test_source, self.test_target, self.train_source, self.train_target]
		if self.baselineFeatures:
			self.train_features = pathToTrainData+"/task1-1_"+self.languagePair+"_training.features"
			self.test_features = pathToTestData+"/task1-1_"+self.languagePair+"_test.features"	
			self.data.extend([self.train_features, self.test_features])	
		if onlyTarget: #only use target data for training and testing
			self.test_source = None
			self.train_source = None
		self.wordDictionary = corpus2dict(self.data)
		self.targetWindowSize = targetWindowSize
		self.sourceWindowSize = sourceWindowSize
		self.labelToInt = {"1":0, "2":1, "3":2} #use this for consistency reasons -> start counting with 0
		self.intToLabel = {y:x for x,y in self.labelToInt.iteritems()}

	def get_train_xy(self):
		ld = self.labelToInt
		print "... extracting context for training data"
		if not self.baselineFeatures:
			ce_train = ContextExtractor1(self.train_source, self.train_target, self.train_score, self.wordDictionary, ld, wordlevel=False)
			train_set_xy = ce_train.extract(self.targetWindowSize,self.sourceWindowSize) #tuple 0:labels, 1:context vectors
		else:
			ce_train = ContextExtractor1(self.train_source, self.train_target, self.train_score, self.wordDictionary, ld, wordlevel=False, features=self.train_features)
			train_set_xy = ce_train.extract(self.targetWindowSize,self.sourceWindowSize)
		train_set_x, train_set_y = shared_dataset(train_set_xy)
		return (train_set_x, train_set_y), ce_train.targetSents


	def get_test_xy(self):
		ld = self.labelToInt
		print "... extracting context for test data"
		if not self.baselineFeatures:
			ce_test = ContextExtractor1(self.test_source, self.test_target, self.test_score, self.wordDictionary, ld, wordlevel=False)
			test_set_xy = ce_test.extract(self.targetWindowSize,self.sourceWindowSize) 
		else:
			ce_test = ContextExtractor1(self.test_source, self.test_target, self.test_score, self.wordDictionary, ld, wordlevel=False, features=self.test_features)
			test_set_xy = ce_test.extract(self.targetWindowSize,self.sourceWindowSize) 
		test_set_x, test_set_y = shared_dataset(test_set_xy)
		return (test_set_x, test_set_y), ce_test.targetSents



class WMT14QETask2(object):
	"""
	Task 2: Word-level
	Predict the quality of the translation for each single word.
	Three levels of granularity: binary, level-1, multi-class
	"""
	def __init__(self, languagePair, pathToTestData, pathToTrainData, targetWindowSize=5, sourceWindowSize=7, onlyTarget=False): #language pair e.g. "EN_DE" or "DE_EN", pathToTestData e.g. "/home/julia/Dokumente/Uni/WS2014/Deep Learning/Prujäkt/WMT14-data/task2_de-en_test", pathToTrainData e.g. "/home/julia/Dokumente/Uni/WS2014/Deep Learning/Prujäkt/WMT14-data/task2_de-en_training"
		self.languagePair = languagePair
		self.test_source = pathToTestData+"/"+self.languagePair+".source.test"
		self.test_target = pathToTestData+"/"+self.languagePair+".tgt_ann.test"
		self.train_source = pathToTrainData+"/"+self.languagePair+".source.train"
		self.train_target = pathToTrainData+"/"+self.languagePair+".tgt_ann.train"
		if onlyTarget: #only use target data for training and testing
			self.test_source = None
			self.train_source = None
		self.data = [self.test_source, self.test_target, self.train_source, self.train_target]
		self.labelToInt_bin = {"OK":0, "BAD":1}
		self.intToLabel_bin = {y:x for x,y in self.labelToInt_bin.iteritems()}

		self.labelToInt_l1 = {"OK":0, "Accuracy":1, "Fluency":2}
		self.intToLabel_l1 = {y:x for x,y in self.labelToInt_l1.iteritems()}

		self.labelToInt_multi = {"OK":0, "Terminology":1, "Mistranslation":2, "Omission":3, "Addition":4, "Untranslated":5, "Accuracy":6, "Style/register":7, "Capitalization":8, "Spelling":9, "Punctuation":10, "Typography":11, "Morphology_(word_form)":12, "Part_of_speech":13, "Agreement":14, "Word_order":15, "Function_words":16, "Tense/aspect/mood":17, "Grammar":18, "Unintelligible":19, "Fluency":20}
		self.intToLabel_multi = {y:x for x,y in self.labelToInt_multi.iteritems()}
		self.wordDictionary = corpus2dict(self.data)

		self.targetWindowSize = targetWindowSize
		self.sourceWindowSize = sourceWindowSize

	def get_train_xy(self,score):
		ld = None
		taskIndex = 0
		if score == "bin":
			ld = self.labelToInt_bin
			taskIndex = 5
		elif score == "l1":
			ld = self.labelToInt_l1
			taskIndex = 4
		elif score == "multi":
			ld = self.labelToInt_multi
			taskIndex = 3
		else:
			raise NameError("Please provide a valid scoring scheme (bin, l1 or multi)")
		print "... extracting context for training data"
		ce_train = ContextExtractor2(self.train_source, self.train_target, self.wordDictionary, ld, taskIndex, True)
		train_set_xy = ce_train.extract(self.targetWindowSize,self.sourceWindowSize) #tuple 0:labels, 1:context vectors
		train_set_x, train_set_y = shared_dataset(train_set_xy)
		return (train_set_x, train_set_y), ce_train.targetWords

	def get_test_xy(self,score):
		ld = None
		taskIndex = 0
		if score == "bin":
			ld = self.labelToInt_bin
			taskIndex = 5
		elif score == "l1":
			ld = self.labelToInt_l1
			taskIndex = 4
		elif score == "multi":
			ld = self.labelToInt_multi
			taskIndex = 3
		else:
			raise NameError("Please provide a valid scoring scheme (bin, l1 or multi)")
		print "... extracting context for test data"
		ce_test = ContextExtractor2(self.test_source, self.test_target, self.wordDictionary, ld, taskIndex, True)
		test_set_xy = ce_test.extract(self.targetWindowSize,self.sourceWindowSize) 
		test_set_x, test_set_y = shared_dataset(test_set_xy)
		return (test_set_x, test_set_y), ce_test.targetWords

def shared_dataset(data_xy, borrow=True):
	""" 
	loads the dataset into Theano shared variables
	"""
	data_x, data_y = data_xy
	shared_x = theano.shared(np.asarray(data_x,dtype='int32'),borrow=borrow)
	shared_y = theano.shared(np.asarray(data_y,dtype=theano.config.floatX), borrow=borrow)
 	return shared_x, T.cast(shared_y, 'int32')

		

from LookUpTableLayer import LookUpTableLayer
from HiddenLayer import HiddenLayer
import theano
import theano.tensor as T
import cPickle


"""
Adaptation of the MLP introduced at the Theano tutorial to the 'NLP from Sratch' approach of Collobert et al.
"""


class NN(object):
	"""
	A feedforward artificial neural network model that has one layer or more of hidden units and nonlinear activations.
	The QUETCH architecture includes a Lookup-Table-Layer, one or more hidden layers and the output is the softmax over all possible labels.
	"""

	def __init__(self, rng, input, n_hidden, n_out, d_wrd, sizeOfDictionary, contextWindowSize, params=None):
		if params == None:
			self.trained = False
		else:
			self.trained = True
			print "...building trained model."


		if self.trained: #loading trained model
			self.lookuptableLayer = LookUpTableLayer(input=input, W=params[0])
			self.hiddenLayer = HiddenLayer(input=self.lookuptableLayer.output, W=params[1], b=params[2])
			self.outputLayer = HiddenLayer(input=self.hiddenLayer.output, W=params[3], b=params[4])

		else:		
			# Lookup Table Layer 
			self.lookuptableLayer = LookUpTableLayer(rng=rng, input=input, d_wrd=d_wrd, sizeOfDictionary=sizeOfDictionary, W=None)

			# HiddenLayer with a tanh activation function
			self.hiddenLayer = HiddenLayer(
				rng=rng,
				input=self.lookuptableLayer.output,
				n_in=d_wrd*contextWindowSize,
				n_out=n_hidden,
				activation=T.tanh
			)
		
			"""
			# in case of using two hidden layers, uncomment this piece of code
			# another HiddenLayer with a tanh activation function, same size as first hidden layer
			self.hiddenLayer2 = HiddenLayer(
				rng=rng,
				input=self.hiddenLayer.output,
				n_in=n_hidden,
				n_out=n_hidden,
				activation=T.tanh
			)
			"""

			# The output layer gets as input the hidden units
			# of the hidden layer
			self.outputLayer = HiddenLayer(
				rng=rng,
				#for two hidden layers use the first of the two following lines
				#input=self.hiddenLayer2.output,
				input=self.hiddenLayer.output,
				n_in=n_hidden,
				n_out=n_out
			)
		
		# log likelihood of the MLP is given by the
		# log likelihood of the output of the model, computed in the
		# output hidden linear layer
		self.log_likelihood = (
			self.outputLayer.log_likelihood
		)

		# same holds for the function computing the number of errors
		self.errors = self.outputLayer.errors

		# the parameters of the model are the parameters of the three layer it is
		# made out of
		if params == None:
			#for 2 hidden layers use the first of the following lines
			#self.params = self.lookuptableLayer.params + self.hiddenLayer.params + self.hiddenLayer2.params + self.outputLayer.params
			self.params = self.lookuptableLayer.params + self.hiddenLayer.params + self.outputLayer.params

		else: #model is already trained
			if len(params) == 5:
				self.lookuptableLayer.params = params[0]
				self.hiddenLayer.params = params[1],params[2]
				self.outputLayer.params = params[3],params[4]
			else:
				print "Not enough parameters given"
				exit(-1)


	def saveParams(self,f):
		""" Save parameters in file """
		out = open(f, 'wb')
		cPickle.dump(self.params, out, -1)
		print "Saved parameters in ", f
		out.close()
		
		

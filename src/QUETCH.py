# -*- coding: UTF-8 -*-

import numpy as np
import theano
import theano.tensor as T
from NN import NN
from ContextExtractor import ContextExtractor1, ContextExtractor2, corpus2dict
from Task import WMT14QETask2, WMT14QETask1_1

import datetime
import time
import codecs

import argparse

from progressbar import ProgressBar

"""QUality Estimation from scraTCH - main class to run experiments"
"""

def writeOutputToFile(task, filename, targets, pred_multi=None, pred_l1=None, pred_bin=None, pred_task1_1=None):
	""" 
	Write the prediction to the specified output file, format according to WMT14 task description: 
	Task1_1:
	 <METHOD NAME> <SEGMENT NUMBER> <SEGMENT SCORE> <SEGMENT RANK>
	(segment rank is always 0, since QUETCH does not perform ranking)
	Task2:	
	<SEGMENT NUMBER> <WORD INDEX> <WORD> <DETAILED SCORE> <LEVEL 1 SCORE> <BINARY SCORE> 
	"""
	f = codecs.open(filename, "w", "utf8")

	if type(task)==WMT14QETask2: #prediction on word level
		#if not predicted, simply return 0 (="OK")
		if pred_multi is None:
			pred_multi = np.zeros(len(targets))
		if pred_l1 is None:
			pred_l1 = np.zeros(len(targets))
		if pred_bin is None:
			pred_bin = np.zeros(len(targets))
		for i,t in enumerate(targets): #information from given target test set
			(Id, index, word, multi, coarse, binary) = t
			#replace gold standard score with system score
			label_bin = task.intToLabel_bin[pred_bin[i]] #lookup in dictionary: from label int to label str
			label_l1 = task.intToLabel_l1[pred_l1[i]]
			label_multi = task.intToLabel_multi[pred_multi[i]]
			outstr = "%s\t%d\t%s\t%s\t%s\t%s\n" % (Id, int(index), word, label_multi, label_l1, label_bin) 
			f.write(outstr)

	elif type(task)==WMT14QETask1_1: #prediction on sentence level
		methodName = "QENN"
		for i,t in enumerate(targets):
			segmentNumber = i+1
			segmentScore = task.intToLabel[pred_task1_1[i]]
			outstr = "%s\t%d\t%s\t%d\n" % (methodName, segmentNumber, segmentScore, 0)
			f.write(outstr)
	else:
		print "No valid task given"
		exit(-1)
	f.close()

def train_and_test_NN(task, x_train, y_train, x_test, y_test, batch_size, numberOfLabels, vocabularySize, contextSize, d_wrd, maxit, threshold, baselineFeatures=False):
	""" Train and test a Neural Network """
	# compute number of minibatches for training
	n_train_batches = x_train.get_value(borrow=True).shape[0] / batch_size
	
	print '... building the model'

	#allocate symbolic variables for the data
	index = T.lscalar()  #index to a minibatch
	x = T.matrix('x', dtype='int32')  #data is matrix, one row for each sample
	y = T.ivector('y')  # the labels are presented as vector of [int] labels 

	rng = np.random.RandomState(1234)
	n_out = numberOfLabels

	#construct the NN
	if baselineFeatures:
		contextSize += 17 #17 baseline features are given

	classifier = NN(rng, x, n_hidden, n_out, d_wrd, vocabularySize, contextSize)

	#symbolic expression for the score we maximize during training is the log likelihood of the model
	score = (classifier.log_likelihood(y))

	#compute the gradient of score with respect to theta
	gparams = [T.grad(score, param) for param in classifier.params]
	
	#SGA update for the parameters
	updates = [ (param, param + learningRate * gparam) for param, gparam in zip(classifier.params, gparams)]

	#compiling a Theano function that returns the score and updates the parameter of the model for the given training data
	train_model = theano.function(
		inputs=[index],
		outputs=score,
		updates=updates,
		givens={
			x: x_train[index * batch_size:(index + 1) * batch_size],
			y: y_train[index * batch_size:(index + 1) * batch_size]
		}
	)
	print "... training"
	pbar = ProgressBar()	
	start_time = time.clock()
	costs = list()
	for it in pbar(range(maxit)):
		itcosts = list()
		for minibatch_index in xrange(n_train_batches):
			minibatch_avg_cost = train_model(minibatch_index)
			#print "\tCurrent cost is ", minibatch_avg_cost
			itcosts.append(minibatch_avg_cost)
		costs.append(np.mean(itcosts))
		if it>1 and (costs[-1]-costs[-2])<threshold: #improvement below threshold
			print "... break: after %d iterations improvement is below threshold of %f"	% (it, threshold)	
			break
	end_time = time.clock()

	print "... training took %.2fm" % ((end_time - start_time) / 60.)

	print "...testing"
	#theano function that returns the prediction 
	predict_model_test = theano.function([], [x, y, classifier.outputLayer.y_pred], givens={x:x_test, y:y_test} )
	x_test,y_test,pred = predict_model_test()
	diff = 0
	total = 0
	zeros = 0
	error = 0
	sqerror = 0
	for i in xrange(len(x_test)):
		total+=1
		if y_test[i] != pred[i]:
			diff+=1
			error += abs(y_test[i]-pred[i])
			sqerror += (y_test[i]-pred[i])**2
		if y_test[i] == 0:
			zeros+=1
	if task == 2:
		print "...accuracy on test set: ",float(total-diff)/total
		#print "...percentage of zeros:", float(zeros)/total
	elif task == 1:
		print "...Mean Absolute Error (MAE) on test set: ",float(error)/total
		print "...Root Mean Squared Error (RMSE) on test set: ", np.sqrt(float(sqerror)/total)
		#print "...percentage of zeros:", float(zeros)/total


	return classifier, costs, pred

def run(task):
	""" Run the training and testing for a given task """
	if task == 1:
		############### TASK 1 ###################################################################
		t1 = WMT14QETask1_1(languagePair, "../WMT14-data/task1-1_"+languagePair+"_test", "../WMT14-data/task1-1_"+languagePair+"_training", targetWindowSize, sourceWindowSize, baselineFeatures=baselineFeatures)

		#print t1.wordDictionary
		vocabularySize = len(t1.wordDictionary)

		###########################################################################################

		print "WMT14 Quality Estimation - Task 1.1"
		print "Language pair:", languagePair
		print "Hyper-parameters: learning rate = %f; maxit = %d; batch_size = %d; n_hidden = %d; d_wrd = %d; targetWindowSize = %d; sourceWindowSize = %d; threshold = %f, baselineFeatures = %r" % (learningRate, maxit, batch_size, n_hidden, d_wrd, targetWindowSize, sourceWindowSize, threshold, baselineFeatures)

		#task 1.1
		numberOfLabels = 3

		#get instance vectors and binary labels for training
		(x_train,y_train),targetSents_train  = t1.get_train_xy()
	
		#get instance vectors and binary labels for testing
		(x_test,y_test),targetSents_test  = t1.get_test_xy()

		classifier, train_scores, pred  = train_and_test_NN(1, x_train, y_train, x_test, y_test, batch_size, numberOfLabels, vocabularySize, contextSize, d_wrd, maxit, threshold, baselineFeatures)

		#print lookup table
		#print classifier.params[0].get_value()
	
		#store parameters in file
		now = str(datetime.datetime.now()).replace(" ","--")
		f = "../parameters/"+now+".params"
		classifier.saveParams(f)

		now = str(datetime.datetime.now()).replace(" ","--")
		fileName = "../results/task1_1/"+languagePair+"/"+now+".results"
		writeOutputToFile(t1, fileName, targetSents_test, pred_task1_1=pred)
		print "...written prediction to file %s." % fileName

	else:
		############### TASK 2 ##################################################################

		pred_bin = None
		pred_multi = None
		pred_l1 = None

		#"translate" language pair notation for task 2
		languagePair2 = languagePair.upper().replace("-","_")		

		t2 = WMT14QETask2(languagePair2, "../WMT14-data/task2_"+languagePair+"_test", "../WMT14-data/task2_"+languagePair+"_training", targetWindowSize, sourceWindowSize)

		#print t2.wordDictionary
		vocabularySize = len(t2.wordDictionary)

		###########################################################################################
		#1a) train NN for binary scores
		#1b) test NN for binary scores
		print "WMT14 Quality Estimation - Task 2"
		print "Language pair:", languagePair
		print "Hyper-parameters: learning rate = %f; maxit = %d; batch_size = %d; n_hidden = %d; d_wrd = %d; targetWindowSize = %d; sourceWindowSize = %d; threshold = %f." % (learningRate, maxit, batch_size, n_hidden, d_wrd, targetWindowSize, sourceWindowSize, threshold)
		print "1) Binary scores"

		numberOfLabels = 2

		#get instance vectors and binary labels for training
		(x_train,y_train),targetWords_train  = t2.get_train_xy("bin")
	
		#get instance vectors and binary labels for testing
		(x_test,y_test),targetWords_test  = t2.get_test_xy("bin")

		classifier_bin, train_scores_bin, pred_bin  = train_and_test_NN(2, x_train, y_train, x_test, y_test, batch_size, numberOfLabels, vocabularySize, contextSize, d_wrd, maxit, threshold)

		#print lookup table
		#print classifier_bin.params[0].get_value()
	
		#print predictions
		#print pred_bin
	
		#store parameters in file
		now = str(datetime.datetime.now()).replace(" ","--")
		f = "../parameters/"+now+".params"
		classifier_bin.saveParams(f)


	
		#############################################################################################

		#2a) train NN for l1-scores
		#2b) test NN for l1-scores
		print "WMT14 Quality Estimation - Task 2.2"
		print "2) Level-1 scores"

		numberOfLabels = 3

		#get instance vectors and l1 labels for training
		(x_train,y_train),targetWords_train  = t2.get_train_xy("l1")
	
		#get instance vectors and l1 labels for testing
		(x_test,y_test),targetWords_test  = t2.get_test_xy("l1")

		classifier_l1, train_scores_l1, pred_l1  = train_and_test_NN(2, x_train, y_train, x_test, y_test, batch_size, numberOfLabels, vocabularySize, contextSize, d_wrd, maxit, threshold)

		#print lookup table
		#print classifier_l1.params[0].get_value()
	
		#print predictions
		#print pred_l1
	
		#store parameters in file
		now = str(datetime.datetime.now()).replace(" ","--")
		f = "../parameters/"+now+".params"
		classifier_l1.saveParams(f)


		#############################################################################################

		#3a) train NN for multi-scores	
		#3b) test NN for multi-scores
		print "WMT14 Quality Estimation - Task 2.3"
		print "3) Multi-label scores"

		numberOfLabels = 21

		#get instance vectors and l1 labels for training
		(x_train,y_train),targetWords_train  = t2.get_train_xy("multi")
	
		#get instance vectors and l1 labels for testing
		(x_test,y_test),targetWords_test  = t2.get_test_xy("multi")

		classifier_multi, train_scores_multi, pred_multi  = train_and_test_NN(2, x_train, y_train, x_test, y_test, batch_size, numberOfLabels, vocabularySize, contextSize, d_wrd, maxit, threshold)

		#print lookup table
		#print classifier_multi.params[0].get_value()
	
		#print predictions
		#print pred_multi
	
		#store parameters in file
		now = str(datetime.datetime.now()).replace(" ","--")
		f = "../parameters/"+now+".params"
		classifier_multi.saveParams(f) 


		#write output to file
		now = str(datetime.datetime.now()).replace(" ","--")
		fileName = "../results/task2/"+languagePair+"/"+now+".results"
		writeOutputToFile(t2, fileName , targetWords_test, pred_multi, pred_l1, pred_bin)
		print "...written prediction to file %s." % fileName
	


if __name__=="__main__":

	#valid language pairs
	validPairs = ["en-es", "es-en", "de-en", "en-de"]

	parser = argparse.ArgumentParser(description="QUality Estimation from scraTCH")
	parser.add_argument("Task", type=int, choices=range(1,3), help="Which WMT14 Quality Estimation shared task to perform")
	parser.add_argument("LanguagePair", type=str, choices=validPairs, help="Source and target language")
	parser.add_argument("-sws", "--SourceWindowSize", type=int, help="Word window size for feature extraction from source text; default for task1: 51, task2: 7")
	parser.add_argument("-tws", "--TargetWindowSize", type=int, help="Word window size for feature extraction from target text; default for task1: 51, task2: 5")
	parser.add_argument("-d", "--WordEmbeddingDimensionality", type=int, help="Dimensionality of feature space trained by Lookup-Table-Layer; default=50")	
	parser.add_argument("-hu", "--HiddenUnits", type=int, help="Number of hidden units in Linear Layer(s); default=300")
	parser.add_argument("-b", "--BaselineFeatures", action="store_true", help="Only applies to Task 1: if True, use WMT14 baseline features for the initialization of the Lookup-Table-Layer; default=False")
	parser.add_argument("-l", "--LearningRate", type=float, help="Learning rate for the Stochastic Gradient Optimization; default=0.001")
	parser.add_argument("-i", "--MaxIt", type=int, help="Convergence criterion: maximum number of training iterations; default=1000")
	parser.add_argument("-t", "--Threshold", type=float, help="Convergence criterion: if reduction of loss below threshold break up training; default=0.0001")
	
	args = parser.parse_args()
	task = args.Task
	languagePair = args.LanguagePair
	learningRate = args.LearningRate
	n_hidden = args.HiddenUnits
	sourceWindowSize = args.SourceWindowSize
	targetWindowSize = args.TargetWindowSize
	d_wrd = args.WordEmbeddingDimensionality
	baselineFeatures = args.BaselineFeatures
	maxit = args.MaxIt
	threshold = args.Threshold
	
 	#set default parameters for optional parameters if not given
	if d_wrd is None:
		d_wrd = 50
	if learningRate is None:
		learningRate = 0.001
	if n_hidden is None:
		n_hidden = 300
	if sourceWindowSize is None:
		if task == 1:
			sourceWindowSize = 51
		elif task == 2:
			sourceWindowSize = 7
	else:
		if sourceWindowSize%2==0:#if even number
			print "Even sourceWindowSize given, must be odd. --> Added +1"
			sourceWindowSize+=1
	if targetWindowSize is None:
		if task == 1:
			targetWindowSize = 51
		elif task == 2:
			targetWindowSize = 5
	else:
		if targetWindowSize%2==0:#if even number
			print "Even targetWindowSize given, must be odd. --> Added +1"
			targetWindowSize+=1
	if baselineFeatures and task != 1:
		print "Baseline features integration only for task 2. Proceeding without baseline features."
	if maxit is None:
		maxit = 1000
	if threshold is None:
		threshold = 0.0001

	batch_size = 1
	contextSize = targetWindowSize+sourceWindowSize

	run(task)
	

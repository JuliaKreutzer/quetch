import cPickle
from NN import NN
import theano.tensor as T
from Task import WMT14QETask2, WMT14QETask1_1
import theano
import datetime
from QUETCH import writeOutputToFile
import numpy as np
import argparse



def loadParams(paramfile):
	""" Load trained parameters from file, build model and evaluate on test set """
	f = open(paramfile,"r")
	params = cPickle.load(f)
	return params

def testModel_task1_1(paramfile, languagePair, testDataDir, trainDataDir, targetWindowSize, sourceWindowSize, outFile, baselineFeatures):
	""" Test given model on task1.1 data """
	#Mean Absolute Error (MAE) (primary metric), Root Mean Squared Error (RMSE).
	t1 = WMT14QETask1_1(languagePair, testDataDir, trainDataDir, targetWindowSize, sourceWindowSize, False, baselineFeatures)
	(x_test,y_test),targetSents_test  = t1.get_test_xy()

	x = T.matrix('x', dtype='int32')  # the data is presented as matrix, one row for each sample
	y = T.ivector('y')  # the labels are presented as vector of [int] labels 

	params = loadParams(paramfile)
	contextSize = targetWindowSize + sourceWindowSize
	if baselineFeatures:
		contextSize += 17

	classifier = NN(None, x, 0, 0, 0, 0, contextSize, params)	

	print "...testing"

	#theano function that returns the prediction 
	predict_model_test = theano.function([], [x, y, classifier.outputLayer.y_pred], givens={x:x_test, y:y_test} )
	x_test,y_test,pred = predict_model_test()
	diff = 0
	total = 0
	zeros = 0
	error = 0
	sqerror = 0
	for i in xrange(len(x_test)):
		total+=1
		if y_test[i] != pred[i]:
			diff+=1
			error += abs(y_test[i]-pred[i])
			sqerror += (y_test[i]-pred[i])**2
		if y_test[i] == 0:
			zeros+=1
	print "...Mean Absolute Error (MAE) on test set: ",float(error)/total
	print "...Root Mean Squared Error (RMSE) on test set: ", np.sqrt(float(sqerror)/total)
	#print "...percentage of zeros:", float(zeros)/total

	if outFile is not None:
		now = str(datetime.datetime.now()).replace(" ","--")
		writeOutputToFile(t1, outFile, targetSents_test, pred_task1_1=pred)
		print "...prediction written to %s." % (outFile)


def testModel_task2(paramfile, task, languagePair, testDataDir, trainDataDir, targetWindowSize, sourceWindowSize, outFile):
	""" Test given model on task 2 data """
	
	t2 = WMT14QETask2(languagePair, testDataDir, trainDataDir, targetWindowSize, sourceWindowSize)
	(x_test,y_test),targetWords_test  = t2.get_test_xy(task)

	x = T.matrix('x', dtype='int32')  # the data is presented as matrix, one row for each sample
	y = T.ivector('y')  # the labels are presented as vector of [int] labels 

	params = loadParams(paramfile)
	classifier = NN(None, x, 0, 0, 0, 0, contextSize, params)	


	print "...testing"

	
	#theano function that returns the prediction 
	predict_model_test = theano.function([], [x, y, classifier.outputLayer.y_pred], givens={x:x_test, y:y_test} )
	x_test,y_test,pred = predict_model_test()
	diff = 0
	total = 0
	zeros = 0
	for i in xrange(len(x_test)):
		total+=1
		if y_test[i] != pred[i]:
			diff+=1
		if y_test[i] == 0:
			zeros+=1
	print "...accuracy on test set: ",float(total-diff)/total
	#print "...percentage of zeros:", float(zeros)/total

	if task == "bin":
		pred_multi = None
		pred_l1 = None
		pred_bin = pred
	elif task == "multi":
		pred_multi = pred
		pred_l1 = None
		pred_bin = None
	elif task == "l1":
		pred_multi = None
		pred_bin = None
		pred_l1 = pred


	if outFile is not None:
		now = str(datetime.datetime.now()).replace(" ","--")
		writeOutputToFile(t2, outFile, targetWords_test, pred_multi, pred_l1, pred_bin)
		print "...prediction written to %s." % (outFile)

if __name__ == "__main__":

	validPairs = ["en-es", "es-en", "de-en", "en-de"]	
	subTasks = ["bin", "l1", "multi", "one"]

	parser = argparse.ArgumentParser(description="QUETCH model evaluation")
	parser.add_argument("Task", type=int, choices=range(1,3), help="Which WMT14 Quality Estimation shared task to evaluate on")
	parser.add_argument("SubTask", type=str, choices=subTasks, help="Only applies to Task 2: subtask to evaluate, for task 1 select 'one'")
	parser.add_argument("LanguagePair", type=str, choices=validPairs, help="Source and target language")
	parser.add_argument("ParameterFile", type=str, help="File which contains trained model parameters")
	parser.add_argument("-sws", "--SourceWindowSize", type=int, help="Word window size for feature extraction from source text; default for task1: 51, task2: 7")
	parser.add_argument("-tws", "--TargetWindowSize", type=int, help="Word window size for feature extraction from target text; default for task1: 51, task2: 5")
	parser.add_argument("-b", "--BaselineFeatures", action="store_true", help="Only applies to Task 1: if True, used WMT14 baseline features for training; default=False")
	parser.add_argument("OutputFile", type=str, help="file where model's predictions are stored")

	args = parser.parse_args()
	task = args.Task
	languagePair = args.LanguagePair
	parameterFile = args.ParameterFile
	sourceWindowSize = args.SourceWindowSize
	targetWindowSize = args.TargetWindowSize
	baselineFeatures = args.BaselineFeatures
	outputFile = args.OutputFile
	subtask = args.SubTask
	if sourceWindowSize is None:
		if task == 1:
			sourceWindowSize = 51
		elif task == 2:
			sourceWindowSize = 7
	if targetWindowSize is None:
		if task == 1:
			targetWindowSize = 51
		elif task == 2:
			targetWindowSize = 5

	contextSize = targetWindowSize+sourceWindowSize
	
	if task==1:
		testModel_task1_1(parameterFile, languagePair, "../WMT14-data/task1-1_"+languagePair+"_test", "../WMT14-data/task1-1_"+languagePair+"_training", targetWindowSize, sourceWindowSize, outputFile, baselineFeatures)	

	else:
		languagePair2 = languagePair.upper().replace("-","_")		
		testModel_task2(parameterFile, subtask, languagePair2, "../WMT14-data/task2_"+languagePair+"_test", "../WMT14-data/task2_"+languagePair+"_training", targetWindowSize, sourceWindowSize, outputFile)

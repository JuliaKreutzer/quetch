# -*- coding: UTF-8 -*-
from ContextExtractor import corpus2dict
import numpy as np
from EvalModel import loadParams
import cPickle
from operator import itemgetter

"""
Inspect the lookup-table parameters:
For each word in the test and trainingsset for a specific task the lookup-table-layer trains a representation in a d_wrd-dimensional feature space.
Which words are most similar according to their representation in the learned feature space?
"""


def dataToDicts1(languagePair, trainPath, testPath, useFeatures=False):
	"""build mappings from words to indices for task 1"""
	#load train and test data (and optional features for task 1.1)
	test_source = testPath+"/"+languagePair+"_source.test"
	test_target = testPath+"/"+languagePair+"_target.test"
	train_source = trainPath+"/"+languagePair+"_source.train"
	train_target = trainPath+"/"+languagePair+"_target.train"
	data = [test_source, test_target, train_source, train_target]
	if useFeatures:
		train_features = trainPath+"/task1-1_"+languagePair+"_training.features"
		test_features = testPath+"/task1-1_"+languagePair+"_test.features"	
		data.extend([train_features, test_features])	
	#create the word dictionary (the creation of the word dictionary is deterministic) that maps words to indices
	wordDictionary = corpus2dict(data) 	
	return wordDictionary


def dataToDicts2(languagePair, trainPath, testPath):
	"""build mappings from words to indices for task 2"""
	#load train and test data
	test_source = testPath+"/"+languagePair+".source.test"
	test_target = testPath+"/"+languagePair+".tgt_ann.test"
	train_source = trainPath+"/"+languagePair+".source.train"
	train_target = trainPath+"/"+languagePair+".tgt_ann.train"
	data = [test_source, test_target, train_source, train_target]
	#create the word dictionary (the creation of the word dictionary is deterministic) that maps words to indices
	wordDictionary = corpus2dict(data) 	
	return wordDictionary




def cosineSim(a,b):
	"""calculate the cosine similarity for a given pair of vectors"""
	dot = np.dot(a,b)
	anorm = np.linalg.norm(a)
	bnorm = np.linalg.norm(b)
	cos = dot/(anorm*bnorm)
	return cos
	

def getTopKSimilarRows(lt,i,k): #i: index of row in lt
	"""get most similar rows to given row"""
	row = lt[i]
	closestPairs = list()
	#find k most similar other rows
	for j,arow in enumerate(lt):
		cs = cosineSim(row,arow)
		if j==i:
			continue
		if len(closestPairs) < k: #list is not filled yet
			closestPairs.append((cs,(i,j))) #add to closest list
			closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
		else: #list contains at least k entries
			if cs > closestPairs[-1][0]: #if higher similarity than last entry in list
				closestPairs.append((cs,(i,j))) #add to closest list
				closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
				closestPairs = closestPairs[:k] #crop list
	return closestPairs

if __name__=="__main__":
	
	#========== TASK 1.1 =======================================================

	trainPath = "WMT14-data/task1-1_de-en_training"
	testPath = "WMT14-data/task1-1_de-en_test"
	languagePair = "de-en"
	useFeatures = False
	paramFile = "parameters/2015-03-14--13:08:03.416263.params"

	k = 10 #find k most similar pairs of words

	wd = dataToDicts1(languagePair, trainPath, testPath, useFeatures=False)
	

	#load the trained lookup table
	lt = loadParams(paramFile)[0].get_value()
	print "Lookup table:", lt, "size: ", lt.shape
	print "Dictionary size: ",len(wd)
	#print wd.token2id	

	#find the most similar rows in the table and their indices
	#similarity measure: cosine
	#check that lookup table and dictionary fit together
	if len(wd) != lt.shape[0]: #rows have to agree
		print "ERROR: lookup table and dictionary do not have the same number of entries."
		exit(-1)
	
	query = u"sudden"
	i = wd.token2id[query]
	similarIndices = getTopKSimilarRows(lt,i,k)

	#translate from indices to words to get the top similar words
	print "Top %d similar words to '%s':" % (k,query)
	for (s,(i,j)) in similarIndices:
		print j, wd.get(j)

	"""
	Sample output:
	Top 10 similar words to 'sudden':
	1178 verurteilt
	2634 against
	2798 self-evident
	5164 8,733
	6190 Constitutional
	6309 haptics
	2986 Anfang
	5330 grim-faced
	2500 reporting
	5529 Black
	"""


	#========== TASK 2 =======================================================

	trainPath = "WMT14-data/task2_de-en_training"
	testPath = "WMT14-data/task2_de-en_test"
	languagePair = "DE_EN"
	useFeatures = False
	paramFile = "parameters/2015-03-14--00:51:56.818805.params"

	k = 10 #find k most similar pairs of words

	wd = dataToDicts2(languagePair, trainPath, testPath)
	

	#load the trained lookup table
	lt = loadParams(paramFile)[0].get_value()
	print "Lookup table:", lt, "size: ", lt.shape
	print "Dictionary size: ",len(wd)
	#print wd.token2id	

	#find the most similar rows in the table and their indices
	#similarity measure: cosine
	#check that lookup table and dictionary fit together
	if len(wd) != lt.shape[0]: #rows have to agree
		print "ERROR: lookup table and dictionary do not have the same number of entries."
		exit(-1)
	
	query = u"classical"
	i = wd.token2id[query]
	similarIndices = getTopKSimilarRows(lt,i,k)

	#translate from indices to words to get the top similar words
	print "Top %d similar words to '%s':" % (k,query)
	for (s,(i,j)) in similarIndices:
		print j, wd.get(j)

	"""
	Sample output:
	Top 10 similar words to 'classical':
	1811 Spiegel
	4936 gets
	2339 Verletzten
	738 denen
	111 Südkalifornien
	1543 improvement
	2772 ab
	1655 register
	14 der
	3609 Zähne
	"""

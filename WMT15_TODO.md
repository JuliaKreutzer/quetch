#TODOs for WMT15#
* adapt to new format
    * new class in Task.py
    * new ContextExtractor
* restrict to binary classification
    * in QUETCH.py
* integrate features (e.g. POS)
    * make list of useful features (inspired from WMT14 submissions)
    * extract features from training and dev data
    * adapt ContextExtractor and options in ContextExtractor.py
* integrate alignment information
    * get alignments for training and dev data
    * adapt ContextExtractor and options in ContextExtractor.py
* find best hyperparameter settings
* get awesome results
* submission until May 25, 2015